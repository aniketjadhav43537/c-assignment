//Simulate string library functions strcpy, strcat, strcmp and strrev.

#include<stdio.h>
#include<string.h>

int main(void)
{
    char str1[30];
    char str2[30];
    int choice;
    do
    {
        printf("\n0. Exit\n1. String Copy\n2. String Concat\n3. String Compare\n4. String Reverse\n");
        printf("Enter choice : ");
        scanf("%d",&choice);
        switch (choice)
        {
        case 1: printf("\nEnter String 1: ");
                scanf("%s",str1);
                strcpy(str2,str1);
                printf("String 1: %s\n");
                printf("Copied String1 into String 2 : %s \n",str2);
               break;
        case 2: printf("\nEnter String 1: ");
                scanf("%s",str1);
                printf("\nEnter String 2: ");
                scanf("%s",str2);
                strcat(str1,str2);
                printf("Concatinated String : %s \n",str1);
               break;
        case 3: printf("\nEnter String 1: ");
                scanf("%s",str1);
                printf("\nEnter String 2: ");
                scanf("%s",str2);
                if(strcmp(str1,str2) == 0)
                     printf("Both the strings are equal\n");
                else
                     printf("Both the strings are not equal\n");
               break;
        case 4: printf("\nEnter String 1: ");
                scanf("%s",str1);
                strrev(str1);
                printf("The Reverse String is : %s\n",str1);
               break;
        }
    } while (choice!=0);
    return 0;
    
}